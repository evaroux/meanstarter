(function() {
    'use strict';

    /* jshint -W098 */

    function TaskController($scope, Global, TaskService, $stateParams, $state, TaskModel) {
        $scope.global = Global;

        // this.$onInit = () =>{
        //     $scope.allTasks();
        // };

        $scope.allTasks = () =>{
            TaskService.getallTasks()
                .then((response)=>{
                    $scope.tasks = response.data;
                })
                .catch((error)=>{
                    TaskModel.handleError(error);
                })
        };

        $scope.form = 'create';
        $scope.addNewTask = () => {
            TaskService.addTask($scope.newTask)
                .then((response) => {
                    $scope.tasks.push(response.data);
                    $scope.newTask = null;//todo
                })
                .catch((error)=>{
                    TaskModel.handleError(error);
                })
        };

        $scope.findTask = () => {
            $scope.form = 'edit';

            TaskService.findTask($stateParams.id)
                .then((response)=>{
                    response.data.created = new Date(response.data.created);
                    $scope.newTask = response.data;
                })
                .catch((error)=>{
                    TaskModel.handleError(error);
                })
        };

        $scope.check = (task) => {
            task = task.task;
            TaskService.editStatusTask(task._id, !task.status)
                .catch((error)=>{
                    TaskModel.handleError(error);
            });
        };

        $scope.editTask = () => {
            TaskService.editTask($stateParams.id, $scope.newTask)
                .then(() => {
                   $state.go('todo_list');
                })
                .catch((error)=>{
                    TaskModel.handleError(error);
                })

        };


        $scope.removeTask = (id) => {
            TaskService.removeTask(id)
                .then((response) => {
                    $scope.tasks = $scope.tasks.filter( ( task ) => {
                        return task._id !== id;
                    } );
                })
                .catch((error)=>{
                    TaskModel.handleError(error);
                })
        };
    }

    angular
        .module('mean.todolist')
        .controller('TaskController', TaskController);

    TaskController.$inject = ['$scope', 'Global', 'TaskService', '$stateParams', '$state', 'TaskModel'];

})();
