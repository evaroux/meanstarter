'use strict';
angular.module('mean.todolist')
    .directive('ngFormTask', () => {
        return {
            restrict : 'E',
            templateUrl: 'todolist/views/form_task.html'
        }
    })
    .directive('ngAlert', () => {
        return {
            restrict : 'E',
            templateUrl: 'todolist/views/alert.html'
        }
    })
;