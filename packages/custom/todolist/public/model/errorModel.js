(function() {
    'use strict';

    function TaskModel($mdDialog) {
        return {
            handleError : (error) => {
                $mdDialog.show(
                    $mdDialog.alert()
                        .title('Erreur')
                        .textContent(error)
                        .ok('Fermer')
                );
            }
        }
    }

    angular
        .module('mean.todolist')
        .factory('TaskModel', TaskModel);

    TaskModel.$inject = ['$mdDialog'];

})();