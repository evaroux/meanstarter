(function() {
    'use strict';

    function Todolist($stateProvider) {
        $stateProvider
            .state('todo_list', {
            url: '/todolist',
            templateUrl: 'todolist/views/list.html'
        }).state('todolist edit', {
            url: '/todolist/edit/:id',
            templateUrl: 'todolist/views/edit.html'
        });
    }

    angular
        .module('mean.todolist')
        .config(Todolist);

    Todolist.$inject = ['$stateProvider'];

})();
