(function() {
    'use strict';

    function TaskService($http, $q) {
        return {
            getallTasks: ()=>{
                let deferred = $q.defer();
                $http.get('/api/tasks')
                    .then((response)=>{
                        deferred.resolve(response);
                    })
                    .catch((response)=>{
                        deferred.reject(response)
                    });
                return deferred.promise;
            },
            addTask: (newTask)=>{
                let deferred = $q.defer();
                $http.post('/api/tasks', newTask)
                    .then((response)=>{
                        deferred.resolve(response);
                    })
                    .catch((response)=>{
                        deferred.reject(response)
                    });
                return deferred.promise;
            },
            findTask: (id)=>{
                let deferred = $q.defer();
                $http.get('/api/tasks/' + id)
                    .then((response)=>{
                        deferred.resolve(response);
                    })
                    .catch((response)=>{
                        deferred.reject(response)
                    });
                return deferred.promise;
            },
            editTask: (id, editTask)=>{
                let deferred = $q.defer();
                $http.put('/api/tasks/' + id, editTask)
                    .then((response)=>{
                        deferred.resolve(response);
                    })
                    .catch((response)=>{
                        deferred.reject(response)
                    });
                return deferred.promise;
            },
            removeTask: (id)=>{
                let deferred = $q.defer();
                $http.delete('/api/tasks/' + id)
                    .then((response)=>{
                        deferred.resolve(response);
                    })
                    .catch((response)=>{
                        deferred.reject(response)
                    });
                return deferred.promise;
            },
            editStatusTask: (id, checked)=>{
                let deferred = $q.defer();
                $http.put('/api/tasks/' + id + '/checked', {'status':checked})
                    .then((response)=>{
                        deferred.resolve(response);
                    })
                    .catch((response)=>{
                        deferred.reject(response)
                    });
                return deferred.promise;
            },
        };
    }

    angular
        .module('mean.todolist')
        .factory('TaskService', TaskService);

    TaskService.$inject = ['$http', '$q'];

})();
