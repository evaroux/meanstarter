'use strict';

let mongoose = require('mongoose'),
    Task = mongoose.model('Task');

exports.list = (request, response)=>{
    Task.find((err, data)=>{
        if(err) return response.send(400);
        response.json(data);
    })
};


exports.create = (request, response)=>{
    let newTask = new Task( request.body );

    newTask.save((err, data)=>{
        if(err) return response.send(400);
        response.json(data);
    });
};

exports.detail = (request, response)=>{
    Task.findOne({_id: request.params.task_id},(err, data)=>{
        if(err) return response.send(400);
        response.json(data);
    })
};


exports.update = (request, response)=>{
    Task.updateOne({_id:request.params.task_id},request.body,(err, data)=>{
        if(err) return response.send(400);
        response.json(data);
    });
};

exports.editChecked = (request, response)=>{
    console.log('editChecked Server');
    Task.updateOne({_id:request.params.task_id},{ $set: {
        status       : request.body.status
    }},(err, data)=>{
        if(err) return response.send(400);
        response.json(data);
    });
};

exports.delete = (request, response)=>{
    Task.findOneAndRemove({_id:request.params.task_id},(err, data)=>{
        if(err) return response.send(400);
        response.send(data);
    })
};