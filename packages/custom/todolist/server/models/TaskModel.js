'use strict';

let mongoose = require('mongoose'),
Schema = mongoose.Schema;

let validate = require('../utils/validate');


//entity
let TasksSchema = new Schema({
    created:{
        type:Date,
        default: Date.now(),
        require: true
    },
    end:{
        type:Date,
        require: false
    },
    title:{
        type: String,
        require: true,
        trim : true
    },
    priority:{
        type: Number,
        require: true,
        validate: [validate.notNull, 'La priorité doit etre comprise entre 1 et 3']
    },
    status:{
        type: Boolean,
        default : false,
        require: true,
        validate: [validate.notNull, 'La priorité doit etre comprise entre 0 et 3']
    },
    user:{
        type:Schema.ObjectId,
        ref: 'User'
    }
});



//export
mongoose.model('Task', TasksSchema);