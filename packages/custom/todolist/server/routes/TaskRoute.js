'use strict';
let taskController = require('../controllers/TaskController');
/* jshint -W098 */
// The Package is past automatically as first parameter
module.exports = (Todolist, app, auth, database, circles) => {

    let requiresAdmin = circles.controller.hasCircle('admin');
    let requiresLogin = circles.controller.hasCircle('authenticated');

    app.get('/api/todolist/example/anyone', function(req, res) {
        res.send('Anyone can access this');
    });

    app.get('/api/todolist/example/auth', requiresLogin, function(req, res) {
        res.send('Only authenticated users can access this');
    });

    app.get('/api/todolist/example/admin', requiresAdmin, function(req, res) {
        res.send('Only users with Admin role can access this');
    });

    app.get('/api/todolist/example/render', function(req, res) {
        Todolist.render('index', {
            package: 'todolist'
        }, function(err, html) {
            //Rendering a view from the Package server/views
            res.send(html);
        });
    });

    app.route('/api/tasks')
        .get(taskController.list)
        .post(taskController.create);

    app.put('api/tasks/:task_id/checked', taskController.editChecked);

    app.route('/api/tasks/:task_id')
        .put(taskController.update)
        .get(taskController.detail)
        .delete(taskController.delete);


};
