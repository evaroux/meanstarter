'use strict';
/**
 * Validations
 */
exports.betweenNumber = (number, min, max) => {
    return ((number > min && number < max));
};

exports.notNull = (input) => {
    return (input === null);
};


exports.notBlank = (input) => {
    return (input === '');
};